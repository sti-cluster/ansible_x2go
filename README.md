Role X2go
=========

Compatiblity
------------

This role can be used only on Ubuntu 18 LTS Other OS will come later.

## Source and Copyright:

This role was downloaded from Ansible Galaxy web site and adapted for our use case

```
debops-contrib.x2go_server - Setup and manage the server-side of X2go

Copyright (C) 2016 Robin Schneider <ypid@riseup.net>
Copyright (C) 2016 DebOps https://debops.org/
```

##List of base packages installed:

```
x2go_server__base_packages:

  - 'x2goserver'
  - 'x2goserver-xsession'
  - 'lxde'  ## Added for our use case -- lightest Desktop Manager
```

```
x2go_server__deploy_state: 'present'
```

Summary of the role variables used
--------------

You can find in defaults/main.yml all variables used in tasks



| Variable                                    | Default Value                                                | Type               | Description                                                  |
| ------------------------------------------- | :----------------------------------------------------------- | :----------------- | ------------------------------------------------------------ |
| `x2go_server__base_packages`                | [],                                                          | String             | package to install                                           |
| `x2go_server__deploy_state`                 | Present                                                      | String             | deploy state                                                 |
| `x2go_server__apt_repo_key_fingerprint_map` | 972FD88FA0BAFB578D0476DFE1F958385BFE2B6E                     | Hexadecimal String | PGP fingerprint map                                          |
| `x2go_server__upstream_release_channel`     | main                                                         | String             | Release builds                                               |
| `x2go_server__upstream_mirror_url`          | ansible_distribution                                         | URL                | URL of upstream APT X2go repository                          |
| `x2go_server__upstream_repository_map`      | {{ x2go_server__upstream_mirror_url }} {{ ansible_distribution_release }} {{ x2go_server__upstream_release_channel }}' | URL                | repository per distribution corresponding to `ansible_distribution`. |
| x2go_server__ppa_release_channel_map:       | Stable                                                       | String             | release type                                                 |


